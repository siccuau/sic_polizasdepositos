object Cuentas: TCuentas
  Left = 0
  Top = 0
  Caption = 'Cuentas'
  ClientHeight = 305
  ClientWidth = 450
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 286
    Width = 450
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitLeft = -892
    ExplicitTop = 223
    ExplicitWidth = 1419
  end
  object grdCuentas: TStringGrid
    Left = 8
    Top = 8
    Width = 434
    Height = 257
    ColCount = 2
    FixedCols = 0
    RowCount = 10
    FixedRows = 0
    TabOrder = 1
    OnDblClick = grdCuentasDblClick
  end
  object qryCuentas: TFDQuery
    Left = 16
    Top = 168
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'DriverID=FB')
    LoginPrompt = False
    Left = 83
    Top = 176
  end
end
