unit UCuentasCo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TFCuentas = class(TForm)
    grdCuentasCo: TDBGrid;
    txtBuscar: TEdit;
    Label1: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Conexion: TFDConnection;
    qryCuentasCo: TFDQuery;
    dsCuentasCo: TDataSource;
    procedure qryCuentasCoAfterScroll(DataSet: TDataSet);
    procedure grdCuentasCoDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtBuscarChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cuenta_id : integer;
  end;

var
  FCuentas: TFCuentas;

implementation

{$R *.dfm}

procedure TFCuentas.FormShow(Sender: TObject);
begin
  with grdCuentasCo do
  begin
    Columns[0].Visible := False;
    Columns[1].Width := 120;
  end;
  txtBuscar.SetFocus;
end;

procedure TFCuentas.grdCuentasCoDblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFCuentas.qryCuentasCoAfterScroll(DataSet: TDataSet);
begin
  cuenta_id := qryCuentasCo.FieldByName('cuenta_id').AsInteger;
end;

procedure TFCuentas.txtBuscarChange(Sender: TObject);
begin
  qryCuentasCo.Filter := 'upper(nombre) like ''%'+txtBuscar.Text+'%'' OR cuenta_pt like ''%'+txtBuscar.Text+'%'''
end;

end.
