unit UCuentas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  Vcl.ComCtrls, FireDAC.Comp.DataSet, Vcl.Grids, Vcl.DBGrids;

type
  TCuentas = class(TForm)
    qryCuentas: TFDQuery;
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    grdCuentas: TStringGrid;
    procedure FormShow(Sender: TObject);
    procedure grdCuentasDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
      Cuenta : String;
  end;

var
  Cuentas: TCuentas;

implementation

{$R *.dfm}

procedure TCuentas.FormShow(Sender: TObject);
begin
grdCuentas.ColWidths[0]:=150;
with grdCuentas do
    begin
    ColWidths[0]:=150;
    Cells[0,0]:='0.00';
    Cells[0,1]:='Subtotal';
    Cells[0,2]:='Descuento';
    Cells[0,3]:='Total';
    Cells[0,4]:='IVA 16%';
    Cells[0,5]:='IVA 8%';
    Cells[0,6]:='IEPS';
    Cells[0,7]:='Base 0%';
    Cells[0,8]:='Base 16%';
    Cells[0,9]:='Base 8%';

    ColWidths[0]:=200;
    Cells[1,0]:='(0.00)';
    Cells[1,1]:='(Subtotal)';
    Cells[1,2]:='(Descuento)';
    Cells[1,3]:='(Total)';
    Cells[1,4]:='(IVA 16%)';
    Cells[1,5]:='(IVA 8%)';
    Cells[1,6]:='(IEPS)';
    Cells[1,7]:='(Base 0%)';
    Cells[1,8]:='(Base 16%)';
    Cells[1,9]:='(Base 8%)';
   end;
end;

procedure TCuentas.grdCuentasDblClick(Sender: TObject);
var
  ARow,
  ACol : Integer;
  Pt : TPoint;
begin
     Pt.X := Mouse.CursorPos.X;
  Pt.Y := Mouse.CursorPos.Y;
  Pt := grdCuentas.ScreenToClient(Pt);

  ACol := grdCuentas.MouseCoord(Pt.X, Pt.Y).X -1;
  ARow := grdCuentas.MouseCoord(Pt.X, Pt.Y).Y;
  Cuenta := grdCuentas.Cells[1,ARow];
  ModalResult := mrOk;

end;

end.
