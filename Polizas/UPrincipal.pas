unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,System.StrUtils, System.Variants, System.Classes, Vcl.Graphics,Math,System.Character,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Vcl.Menus,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc, Dateutils,UClientes, UPlantillas, UPreferencias,System.IOUtils,
  Vcl.ExtCtrls, UProveedores, Vcl.WinXPickers;




type
  TFPrincipal = class(TForm)
    grdCFDI: TDBGrid;
    dtpInicio: TDateTimePicker;
    dtpFin: TDateTimePicker;
    btnBuscar: TButton;
    txtBusqueda: TEdit;
    Label1: TLabel;
    Conexion: TFDConnection;
    MainMenu1: TMainMenu;
    Configuracin1: TMenuItem;
    qryCFDI: TFDQuery;
    dsCFDI: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    btnGenerarTodas: TButton;
    Preferencias1: TMenuItem;
    qryPlantilla: TFDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    qryPlantillaCREACION: TSQLTimeStampField;
    StatusBar1: TStatusBar;
    lblNumComprobantes: TLabel;
    ProgressBar1: TProgressBar;
    qryPlantillaCLASIF: TStringField;
    qryPlantillaFORMULA_DEBE: TStringField;
    qryPlantillaFORMULA_HABER: TStringField;
    qryCargos: TFDQuery;
    qryCobros: TFDQuery;
    qryCobrosDeposito: TFDQuery;
    qryDepositos: TFDQuery;
    qryXML: TFDQuery;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure GeneraPoliza;
    procedure btnGenerarTodasClick(Sender: TObject);
    procedure Plantillas1Click(Sender: TObject);
    procedure Preferencias1Click(Sender: TObject);
    function generapolizadocto(metodo_pago:string) : integer;
    {function generapolizacredito : integer;}
    function GetCuenta(valor : string): integer;
    function GetValor(valor : string):double;
     Function ConvertirFormula (valor:string): String;
     Function ListaResultado (lista:TStringList): Double;
     function removeLeadingZeros(const Value: string): string;
    procedure CreaDetalle(poliza_id:integer;cta_id:integer;debe:double;haber:double;folio_xml:string);
     procedure CreaDetalle1(poliza_id:integer;cta_id:integer;debe:double;haber:double;folio_xml:string);
    procedure grdCFDIDblClick(Sender: TObject);
    procedure CreaTablas;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbTipoChange(Sender: TObject);
    procedure actualizarvista;
    function filtros:string;
    procedure cbVentasClick(Sender: TObject);
    procedure cbComprasClick(Sender: TObject);
    procedure cbPagosClientesClick(Sender: TObject);
    procedure cbPagoProveedoresClick(Sender: TObject);
    procedure cbNominaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure grdCFDIDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure txtBusquedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Proveedores1Click(Sender: TObject);
    procedure cbCreaPolizasChange(Sender: TObject);
    procedure dtpInicioChange(Sender: TObject);
    procedure creartodas;
    procedure CrearTodaspordia;
    procedure cbNotasCreditoCXCClick(Sender: TObject);
    procedure cbNotasCreditoCxpClick(Sender: TObject);
    procedure dtpInicioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryDepositosAfterOpen(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
    subtotal, descuento, descuento0, descuento16, descuento8, total, impuestos, iva0,iva8,iva16,ieps,
  v0,v16,v8,total_pago,bieps,tasa_iva,tasa_ieps : double;
    rfc_cliente,rfc_emisor, tipo_comp,descripcion_poliza, fecha_poliza_str,folio_deposito,clasif,folio_cobro,folio_cobro1,folio_cargo : string;
    creadas,poliza_id,co_cfd_id,NF,id_deposito,id_cobro,banco_cfdi_id,banco_det_id : integer;
    log : TextFile;
  protected
    procedure CreateParams(var Params :TCreateParams); override;
  end;

var
  FPrincipal: TFPrincipal;

implementation

{$R *.dfm}

procedure TFPrincipal.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.ExStyle := Params.ExStyle OR WS_EX_APPWINDOW;
end;

procedure TFPrincipal.dtpInicioChange(Sender: TObject);
begin
  if dtpFin.Visible = False then
  begin
    dtpFin.Date := dtpInicio.Date;
  end;
end;

procedure TFPrincipal.dtpInicioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  nf := NF + 1;
  if NF = 2 then
  begin
    dtpInicio.Perform(WM_CHAR, VK_RIGHT, 1);
    nf := 0;
  end;
end;

function TFPrincipal.Generapolizadocto(metodo_pago:string) : integer;
var
  tipo_poliza_id,mes,anio,tipo_poliza_det_id,consec,
  xml_id,cta_id,validacion : integer;
  poliza_existe : boolean;
  tipo_cons,prefijo_poliza,query_str, folio_poliza, folio_xml,
  tipo_plantilla, descripcion : string;
  valor_debe,valor_haber : double;
begin
  folio_xml := qryCFDI.FieldByName('folio').AsString;
 // folio:=removeLeadingZeros(folio_xml);
  //  xml_id := Conexion.ExecSQLScalar('select cfdi_id from repositorio_cfdi where folio=:f',[folio]);
  //xml_id := qryCFDI.FieldByName('cfdi_id').AsInteger;
  // VALIDA SI YA ESTA LA CONFIGURACION
  if (Conexion.ExecSQLScalar('select tipo_poliza_contado_id from sic_plantillas_config') = null) or
      (Conexion.ExecSQLScalar('select tipo_poliza_credito_id from sic_plantillas_config') = null) then
    Result := 0
  else
  begin
 { clasif:=Conexion.ExecSQLScalar('select (case when (tipo_comprobante=''I'' and naturaleza=''E'') then ''Venta'''+
            ' when (tipo_comprobante=''I'' and naturaleza=''R'') then ''Compra'''+
            ' when (tipo_comprobante=''P'' and naturaleza=''E'') then ''Pago de cliente'''+
            ' when (tipo_comprobante=''P'' and naturaleza=''R'') then ''Pago a proveedor'''+
            ' when (tipo_comprobante=''E'' and naturaleza=''E'') then ''Nota de credito cxc'''+
            ' when (tipo_comprobante=''E'' and naturaleza=''R'') then ''Nota de credito cxp'''+
            ' when (tipo_comprobante=''N'' and naturaleza=''E'') then ''Nomina'''+
      ' end) as clasif from repositorio_cfdi where folio=:f',[folio]);  }
    //OBTIENE TIPO DE POLIZA
    if clasif = 'Venta' then
    begin
      if metodo_pago = 'PUE' then
        tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_contado_id from sic_plantillas_config')
      else
        tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_credito_id from sic_plantillas_config');
    end
    else if clasif = 'Compra' then
    begin
      if metodo_pago = 'PUE' then
        tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_compras_contado_id from sic_plantillas_config')
      else
        tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_compras_credito_id from sic_plantillas_config');
    end
    else if clasif = 'Pago de cliente' then
    begin
      tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_pagocxc_id from sic_plantillas_config');
    end
    else if clasif = 'Pago a proveedor' then
    begin
      tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_pagocxp_id from sic_plantillas_config');
    end
    else if clasif = 'Nomina' then
    begin
      tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_nomina_id from sic_plantillas_config');
    end
    else if clasif = 'Nota de credito cxc' then
    begin
      tipo_poliza_id := Conexion.ExecSQLScalar('select TIPO_POLIZA_NCCXC_ID from sic_plantillas_config');
    end
    else if clasif = 'Nota de credito cxp' then
    begin
      tipo_poliza_id := Conexion.ExecSQLScalar('select TIPO_POLIZA_NCCXp_ID from sic_plantillas_config');
    end;


    if Conexion.ExecSQLScalar('select prefijo from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]) = null then
      prefijo_poliza := ''
    else
      prefijo_poliza := Conexion.ExecSQLScalar('select prefijo from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]);

    tipo_cons := Conexion.ExecSQLScalar('select tipo_consec from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]);
    if tipo_cons = 'M' then
    begin
      mes := Monthof(qryCFDI.FieldByName('fecha').AsDateTime);
      anio := YearOf(qryCFDI.FieldByName('fecha').AsDateTime);
      tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                      ' where tipo_poliza_id = :pid and mes=:mes and ano=:a',[tipo_poliza_id,mes,anio]);
      if tipo_poliza_det_id = 0 then
      begin
       tipo_poliza_det_id := Conexion.ExecSQLScalar('insert into tipos_polizas_det values(-1,:pid,:a,:m,1) returning tipo_poliza_det_id',[tipo_poliza_id,anio,mes]);
      end;
    end;
    if tipo_cons = 'E' then
    begin
      anio := YearOf(qryCFDI.FieldByName('fecha').AsDateTime);
      tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                      ' where tipo_poliza_id = :pid and  ano=:a',[tipo_poliza_id,anio]);
    end;
    if tipo_cons = 'P' then
    begin
      tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                      ' where tipo_poliza_id = :pid',[tipo_poliza_id]);
    end;
    consec := Conexion.ExecSQLScalar('select consecutivo from tipos_polizas_det where tipo_poliza_det_id = :tp',[tipo_poliza_det_id]);
    folio_poliza := prefijo_poliza + Format('%.*d',[9-(prefijo_poliza.length), consec]);

    //VALIDA SI YA EXISTE LA POLIZA (SI ES EGRESO VALIDA LA DESCRIPCION, SI NO VALIDA LA REFERENCIA)
    if (clasif = 'Compra') or (clasif = 'Pago a proveedor') then
      validacion := Conexion.ExecSQLScalar('select count (distinct docto_co_id) from doctos_co_det where descripcion=:r',[folio_xml])
    else
      validacion := Conexion.ExecSQLScalar('select count (distinct docto_co_id) from doctos_co_det where refer=:r',[folio_xml]);


    //SI EXISTE CON LOS CRITERIOS ANTERIORES Y SI ES LA OPERACION POR MOVIMIENTO
    if (validacion > 0) then
    begin
      poliza_id := Conexion.ExecSQLScalar('select distinct docto_co_id from doctos_co_det where descripcion=:r',[folio_xml]);
      poliza_existe := True;
    end
    else
    begin
        //SI LAS VA A GENERAR POR MOVIMIENTO
         // descripcion := Quotedstr('Factura:'+folio_xml+sLineBreak+Conexion.ExecSQLScalar('select nombre from repositorio_cfdi where folio=:f',[folio]));


        //SE VALIDA LA DESCRIPCION PARA SABER SI SE TIENE QUE GENERAR UN NUEVO ENCABEZADO EN MOVIMIENTOS POR DIA
        if descripcion <> descripcion_poliza then
        begin
          {***************************INSERTA EL ENCABEZADO DE LA POLIZA*******************************}
          query_str := 'insert into doctos_co(docto_co_id,tipo_poliza_id,poliza,fecha,moneda_id,tipo_cambio,estatus,sistema_origen,descripcion,aplicado)'+
                        ' values(-1,'+inttostr(tipo_poliza_id)+','+QuotedStr(folio_poliza)+','''+FormatDateTime('mm/dd/yyyy',qryCFDI.FieldByName('fecha').AsDateTime)+''',1,1,''N'',''CO'','+descripcion+',''N'') returning docto_co_id';
          poliza_id := Conexion.ExecSQLScalar(query_str);
          {**************************ACTUALIZA EL DETALLE DEL TIPO DE POLIZA***************************}
          Conexion.ExecSQL('update tipos_polizas_det set consecutivo = consecutivo+1 where tipo_poliza_det_id = :t',[tipo_poliza_det_id]);
          poliza_existe := False;
          descripcion_poliza := descripcion;
          fecha_poliza_str := Formatdatetime('dd/mm/yyyy',qryCFDI.FieldByName('fecha').AsDateTime);
        end;

        {************************LIGA EL CFDI DE LA FACTURA A LA POLIZA RECIEN CREADA****************}
          co_cfd_id := Conexion.ExecSQLScalar('insert into doctos_co_cfdi values(-1,:p,:qr,''N'') returning docto_co_cfdi_id',[poliza_id,xml_id]);
          Conexion.Commit;
      
        // OBTIENE EL TIPO DE PLANTILLA (CONTADO O CREDITO)
        if (clasif = 'Venta') or (clasif = 'Compra') then
        begin
          if metodo_pago = 'PUE' then
            tipo_plantilla := 'C'
          else if metodo_pago = 'PPD' then
            tipo_plantilla := 'R';
        end;

        //RECORRIDO DE LA PLANTILLA
        with qryPlantilla do
        begin
          //SELECCIONA LOS DETALLES DE LA PLANTILLA CORRESPONDIENTE
          if tipo_plantilla = '' then
          begin
            qryPlantilla.Filtered := False;
          end
          else
          begin
            qryPlantilla.Filter := 'tipo = '''+tipo_plantilla+'''';
            qryPlantilla.Filtered := True;
          end;

          ParamByname('clasif').AsString := clasif;
          Refresh;
          first;
          while not eof do
          begin
            cta_id := GetCuenta(FieldByName('cta').AsString);
            if Conexion.ExecSQLScalar('select count(formula_debe) from sic_plantillas_co_det where cta=:c and clasif=:cla and tipo=:t',[FieldByName('cta').AsString,clasif,tipo_plantilla])>0 then
            begin
            valor_debe := GetValor(FieldByname('formula_debe').AsString);
            end
            else valor_debe := GetValor(FieldByname('debe').AsString);

            if Conexion.ExecSQLScalar('select count(formula_haber) from sic_plantillas_co_det where cta=:c and clasif=:cla and tipo=:t',[FieldByName('cta').AsString,clasif,tipo_plantilla])>0 then
            begin
            valor_haber := GetValor(FieldByname('formula_haber').AsString);
            end
            else valor_haber := GetValor(FieldByname('haber').AsString);

            if (valor_haber > 0) or (valor_debe > 0) then
            begin
              if cta_id = 0 then
              begin
                WriteLn(log, qryCFDI.FieldByName('folio').AsString+'--'+qryCFDI.FieldByName('nombre').AsString+'--'+rfc_cliente+'(No hay cuenta de '+FieldByName('cta').AsString+')');
              end
              else
              begin
                CreaDetalle(poliza_id,cta_id,valor_debe,valor_haber,folio_xml);
                Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_id]);
              end;
            end;
            Next;
          end;
        end;

          try
            Conexion.ExecSQL('update doctos_co set aplicado=''S'' where docto_co_id = :pid',[poliza_id]);
            Conexion.ExecSQL('execute procedure aplica_docto_co(:docto_id,''A'')',[poliza_id]);
            Conexion.Commit;

          except
            result := 0;
          end;

    end;
    if poliza_existe then
      Result := 0
    else
      Result := 1;
  end;

end;

procedure TFPrincipal.CreaDetalle(poliza_id:integer;cta_id:integer;debe:double;haber:double;folio_xml:string);
var
    tipo_as,query_str : string;
    importe : double;
    det_id : integer;
    tasa_iva_int,tasa_ieps_int:integer;
begin
  //
  tasa_iva_int:=Trunc(tasa_iva*100);
  tasa_ieps_int:=Trunc(tasa_ieps*100);
  if debe>0 then
  begin
    tipo_as := 'C';
    importe := debe;
  end
  else
  begin
    tipo_as := 'A';
    importe := haber;
  end;
  if (clasif = 'Compra') or (clasif = 'Pago a proveedor') then
  begin
    query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''+folio_xml+''',-1,1,''S'') returning docto_co_det_id';
  end
  else
  begin
    //SI NO TIENE CARGOS (VENTAS AL PUBLICO)
    if qryCargos.RecNo=0 then
    begin
      //SI ES IEPS SIN CARGOS
      if cta_id=Conexion.ExecSQLScalar('select cta_ieps_id from sic_cuentas_co_gen_clientes') then
        begin
        query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''''',''IEPS '+inttostr(tasa_ieps_int)+' %'',-1,1,''S'') returning docto_co_det_id';
        end
        // SI ES IVA SIN CARGOS
        else if cta_id=Conexion.ExecSQLScalar('select cta_iva_pagado_id from sic_cuentas_co_gen_clientes') then
        begin
        query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''''',''IVA '+inttostr(tasa_iva_int)+' %'',-1,1,''S'') returning docto_co_det_id';
        end
        else
        begin
                query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''','''',-1,1,''S'') returning docto_co_det_id';
        end;
    end
    else
    begin
    //SI TIENE CARGOS
        //SI ES IEPS
      if cta_id=Conexion.ExecSQLScalar('select cta_ieps_id from sic_cuentas_co_gen_clientes') then
        begin
              query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''+folio_xml+''','''+Conexion.ExecSQLScalar('select nombre from repositorio_cfdi where folio=:f',[folio_cargo])
                +', IEPS '+inttostr(tasa_ieps_int)+' %'',-1,1,''S'') returning docto_co_det_id';
        end
        //SI ES IVA
       else if cta_id=Conexion.ExecSQLScalar('select cta_iva_pagado_id from sic_cuentas_co_gen_clientes') then
        begin
                query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''+folio_xml+''','''+Conexion.ExecSQLScalar('select nombre from repositorio_cfdi where folio=:f',[folio_cargo])
                +', IVA '+inttostr(tasa_iva_int)+' %'',-1,1,''S'') returning docto_co_det_id';
        end
        else
        begin
        query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                    ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                    ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                    ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''+folio_xml+''','''+Conexion.ExecSQLScalar('select nombre from repositorio_cfdi where folio=:f',[folio_cargo])
                    +''',-1,1,''S'') returning docto_co_det_id';
        end;
    end;
  end;
    det_id := Conexion.ExecSQLScalar(query_str,[cta_id]);
    Conexion.Commit;
    //ENLAZA EL XML CON EL ASIENTO SI EXISTE
    if co_cfd_id<>0 then
    begin
    if Conexion.ExecSQLScalar('select count(docto_co_det_id) from DOCTOS_CO_DET_CFDI where docto_co_det_id=:id and docto_co_cfdi_id=:id2',[banco_det_id,co_cfd_id])=0 then
    Conexion.ExecSQL('insert into DOCTOS_CO_DET_CFDI values(:det,:co_cfdi)',[banco_det_id,co_cfd_id]);
    Conexion.ExecSQL('insert into DOCTOS_CO_DET_CFDI values(:det,:co_cfdi)',[det_id,co_cfd_id]);
    Conexion.Commit;
    end;

end;

procedure TFPrincipal.CreaDetalle1(poliza_id:integer;cta_id:integer;debe:double;haber:double;folio_xml:string);
var
    tipo_as,query_str : string;
    importe : double;
    det_id : integer;
begin
  //
  if debe>0 then
  begin
    tipo_as := 'C';
    importe := debe;
  end
  else
  begin
    tipo_as := 'A';
    importe := haber;
  end;
  if (clasif = 'Compra') or (clasif = 'Pago a proveedor') then
  begin
    query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''+folio_xml+''',-1,1,''S'') returning docto_co_det_id';
  end
  else
  begin
    query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                ','''+tipo_as+''','+ floattostr(importe)+','+floattostr(importe)+','''+folio_xml+''','''+Conexion.ExecSQLScalar('select nombre from repositorio_cfdi where folio=:f',[folio_deposito])
                +''',-1,1,''S'') returning docto_co_det_id';
  end;
    banco_det_id := Conexion.ExecSQLScalar(query_str,[cta_id]);
    Conexion.Commit;
    //ENLAZA EL XML CON EL ASIENTO
   // Conexion.ExecSQL('insert into DOCTOS_CO_DET_CFDI values(:det,:co_cfdi)',[det_id,co_cfd_id]);
   // Conexion.Commit;

end;

procedure TFPrincipal.grdCFDIDblClick(Sender: TObject);
begin

//    try
      AssignFile(log, 'log.txt');
      Rewrite(log);
      WriteLn(log,formatdatetime('dd/mm/yyyy t',Now));
      GeneraPoliza;
         if Formatdatetime('dd/mm/yyyy',qryCFDI.FieldByName('fecha').AsDateTime) <> fecha_poliza_str then
      begin
        try
        Conexion.ExecSQL('update doctos_co set aplicado=''S'' where docto_co_id = :pid',[poliza_id]);
        Conexion.ExecSQL('execute procedure aplica_docto_co(:docto_id,''A'')',[poliza_id]);
        Conexion.Commit;
        except
        Conexion.ExecSQL('update doctos_co set estatus = ''P'' where docto_co_id = :di',[poliza_id]);
       end;
      end;
      CloseFile(log);
      ShowMessage('Proceso terminado correctamente. '+inttostr(creadas)+' p�lizas creadas');
      Creadas := 0;
      actualizarvista;
//    except
//      CloseFile(log);
//    end;

end;

procedure TFPrincipal.grdCFDIDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
 DrawState: Integer;
 DrawRect: TRect;
begin

end;

function TFPrincipal.GetValor(valor : string):double;
var
  listaformula:TStringList;
  temp:String;
  i: Integer;
begin
listaformula:=TStringList.Create;
  //OBTIENE EL VALOR DEPENDIENDO DE LA PLANTILLA
  if valor = '0.00' then
  begin
    Result := 0;
  end
  else if valor = 'Subtotal' then
  begin
    Result := subtotal;
  end
  else if valor = 'Descuento' then
  begin
    Result := descuento;
  end
  else if valor = 'Total' then
  begin
    if (clasif = 'Pago de cliente') or
       (clasif = 'Pago a proveedor') then
      Result := total_pago
    else
      Result := total;
  end
  else if valor = 'IVA 16%' then
  begin
    result := iva16
  end
  else if valor = 'IVA 8%' then
  begin
    result := iva8;
  end
  else if valor = 'IEPS' then
  begin
    result := ieps;
  end
  else if valor = 'Base 0%' then
  begin
    result := v0;
  end
  else if valor = 'Base 16%' then
  begin
    Result := v16;
  end
  else if valor = 'Base 8%' then
  begin
    Result := v8;
  end
  //EN CASO DE SER FORMULA
  else if valor[1]='(' then
  begin
  //DECODIFICAR FORMULA
  valor:=ConvertirFormula(valor);   //CONVIERTE LA FORMULA ELIMINANDO PARENTESIS Y CONVIRTIENDO A VARIABLES

  for i := 1 to valor.Length do
    begin
    if i=valor.Length then
      begin
      temp:=temp+valor[i];
      listaformula.Add(temp);
      end
       else if (valor[i]='+') or (valor[i]='-') or (valor[i]='*') or (valor[i]='/') then
      begin
      listaformula.Add(temp);
      listaformula.Add(valor[i]);
      temp:='';
      end
      else
      begin
      temp:=temp+valor[i];
      end;
    end;
          //OBTENER RESULTADO CON LISTAS DE FORMULAS
          Result:=ListaResultado(listaformula);
    end
  else
  begin
    Result := 0;
  end;
end;

Function TFPrincipal.ListaResultado (lista:TStringList): Double;
var j:integer; resultadofinal,operando:double;
 Begin
  for j := 0 to lista.Count-1 do
    begin
      if (lista[j]<>'+') and (lista[j]<>'-') and (lista[j]<>'*') and (lista[j]<>'/') then
        begin
          if lista[j]='0' then begin operando:=0; end
          else if lista[j]='subtotal' then begin operando:=subtotal; end
          else if lista[j]='descuento' then begin operando:=descuento; end
          else if lista[j]='total_pago' then begin operando:=total_pago; end
          else if lista[j]='total' then begin operando:=total; end
          else if lista[j]='iva16' then begin operando:=iva16; end
          else if lista[j]='iva8' then begin operando:=iva8; end
          else if lista[j]='ieps' then begin operando:=ieps; end
          else if lista[j]='v0' then begin operando:=v0; end
          else if lista[j]='v16' then begin operando:=v16; end
          else if lista[j]='v8' then begin operando:=v8; end
          else operando:=strtofloat(lista[j]);
          end;

          if j>0 then
         begin
          if lista[j-1]='*' then
          begin
          resultadofinal:=resultadofinal*operando;
          end
          else if lista[j-1]='/' then
          begin
          resultadofinal:=resultadofinal/operando;
          end
          else if lista[j-1]='+' then
          begin
          resultadofinal:=resultadofinal+operando;
          end
          else if lista[j-1]='-' then
          begin
          resultadofinal:=resultadofinal-operando;
          end
         end
           else
            begin
            resultadofinal:=operando;
            end;
    end;
    Result:=resultadofinal;
 End;

 Function TFPrincipal.ConvertirFormula (valor:string): String;
 Begin
 	valor:=StringReplace(valor,'(0.00)','0',[rfReplaceAll]);
  valor:=StringReplace(valor,'(Subtotal)','subtotal',[rfReplaceAll]);
  valor:=StringReplace(valor,'(Descuento)','descuento',[rfReplaceAll]);
   if (qryCfdi.FieldByName('clasif').AsString = 'Pago de cliente') or
       (qrycfdi.FieldByName('clasif').AsString = 'Pago a proveedor') then
       begin
       valor:=StringReplace(valor,'(Total)','total_pago',[rfReplaceAll]);
       end
       else valor:=StringReplace(valor,'(Total)','total',[rfReplaceAll]);
  valor:=StringReplace(valor,'(IVA 16%)','iva16',[rfReplaceAll]);
  valor:=StringReplace(valor,'(IVA 8%)','iva8',[rfReplaceAll]);
  valor:=StringReplace(valor,'(IEPS)','ieps',[rfReplaceAll]);
  valor:=StringReplace(valor,'(Base 0%)','v0',[rfReplaceAll]);
  valor:=StringReplace(valor,'(Base 16%)','v16',[rfReplaceAll]);
  valor:=StringReplace(valor,'(Base 8%)','v8',[rfReplaceAll]);
 	Result :=  valor;
 End;

function TFPrincipal.GetCuenta(valor : string): integer;
var
  cliente_id : integer;
  rfc : string;
begin
  //OBTIENE EL ID DE LA CUENTA CONTABLE DEPENDIENDO DE LA PLANTILLA
  if valor = 'Cuenta Bancos General' then
  begin
    if Conexion.ExecSQLScalar('select cta_bancos_general_id from sic_plantillas_config') = null then
    begin

    end
    else
    begin
      Result := Conexion.ExecSQLScalar('select cta_bancos_general_id from sic_plantillas_config');
    end;
  end;

  //SI ES COMPROBANTE EMITIDO
 { if Conexion.ExecsqlScalar('select naturaleza from repositorio_cfdi where folio=:f',[folio]) = 'E' then
  begin
    rfc := rfc_cliente;
    if Conexion.ExecSQLScalar('select coalesce(tercero_co_id,0) from terceros_co where rfc = :rfc',[rfc]) = 0 then
      cliente_id := 0
    else
      cliente_id := Conexion.ExecSQLScalar('select coalesce(tercero_co_id,0) from terceros_co where rfc = :rfc',[rfc]);

    if valor = 'Cuenta Clientes' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(cuenta_cxc_id,0) from terceros_co where rfc=:rfc',[rfc_cliente]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CUENTA_CLIENTES_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(cuenta_cxc_id,0) from terceros_co where rfc=:rfc',[rfc_cliente]);
      end;
    end;
    if valor = 'Ventas 0% credito' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS0_CREDITO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS0_CREDITO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS0_CREDITO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Ventas 0% contado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS0_CONTADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS0_CONTADO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS0_CONTADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Ventas 16% credito' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS16_CREDITO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS16_CREDITO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS16_CREDITO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Ventas 16% contado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS16_CONTADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS16_CONTADO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS16_CONTADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Ventas 8% credito' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS8_CREDITO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS8_CREDITO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS8_CREDITO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Ventas 8% contado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS8_CONTADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS8_CONTADO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_VENTAS8_CONTADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'IVA Pagado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PAGADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PAGADO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PAGADO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'IVA Pendiente' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PENDIENTE_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PENDIENTE_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PENDIENTE_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'IEPS' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_IEPS_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IEPS_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IEPS_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Descuento' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_DESCUENTO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_DESCUENTO_ID,0) from sic_cuentas_co_gen_clientes');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_DESCUENTO_ID,0) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]);
      end;
    end;
  end;  }


  //PROVEEDORES
 { if Conexion.ExecsqlScalar('select naturaleza from repositorio_cfdi where folio=:f',[folio]) = 'R' then
  begin
    rfc := rfc_emisor;
    if Conexion.ExecSQLScalar('select coalesce(tercero_co_id,0) from terceros_co where rfc = :rfc',[rfc]) = 0 then
      cliente_id := 0
    else
      cliente_id := Conexion.ExecSQLScalar('select coalesce(tercero_co_id,0) from terceros_co where rfc = :rfc',[rfc]);

    if valor = 'Cuenta proveedores' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(cuenta_cxp_id,0) from terceros_co where rfc=:rfc',[rfc]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_PROVEEDORES_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(cuenta_cxp_id,0) from terceros_co where rfc=:rfc',[rfc]);
      end;
    end;
    if valor = 'Compras 0% credito' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS0_CREDITO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS0_CREDITO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS0_CREDITO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Compras 0% contado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS0_CONTADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS0_CONTADO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS0_CONTADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Compras 16% credito' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS16_CREDITO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS16_CREDITO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS16_CREDITO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Compras 16% contado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS16_CONTADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS16_CONTADO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS16_CONTADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Compras 8% credito' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS8_CREDITO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS8_CREDITO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS8_CREDITO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Compras 8% contado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS8_CONTADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS8_CONTADO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_COMPRAS8_CONTADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'IVA Pagado' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PAGADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PAGADO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PAGADO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'IVA Pendiente' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PENDIENTE_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PENDIENTE_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IVA_PENDIENTE_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'IEPS' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_IEPS_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IEPS_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_IEPS_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
    if valor = 'Descuento' then
    begin
      if Conexion.ExecSQLScalar('select coalesce(CTA_DESCUENTO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]) = 0 then
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_DESCUENTO_ID,0) from SIC_CUENTAS_CO_GEN_PROVEEDORES');
      end
      else
      begin
        Result := Conexion.ExecSQLScalar('select coalesce(CTA_DESCUENTO_ID,0) from sic_cuentas_co_proveedores where proveedor_id = :cid',[cliente_id]);
      end;
    end;
  end;     }
end;

procedure TFprincipal.GeneraPoliza;
var
  xml,xmlnuevo, metodo_pago_sat,folio,query_str,prefijo_poliza,tipo_cons,
  folio_poliza,descripcion,folio_xml,tipo_plantilla,referencia: string;
  doc: IXMLDocument;
  comprobante,n_imp,tras,imp, concepto,complemento,pago: IXMLNode;
  conceptos, pagos : IXMLNodeList;
  i: Integer;
  j,genpol,id_cargo,tipo_poliza_id,tipo_poliza_det_id,mes,anio,consec,cta_id,xml_id: Integer;
  k: Integer;
  bieps_concepto,c0,c16,c8,desc_c,importe_cobro,importe_cargo,impuesto_cargo,
  impuesto_proporcional,valor_debe,valor_haber,importe_deposito,importe_cargo_total,resultado : double;
  letra:Char;
begin
  genpol := 0;
  ieps := 0;
  iva16 := 0;
  v16 := 0;
  iva0 := 0;
  iva8 := 0;
  v0 := 0;
  v8 := 0;
  descuento := 0;
  total_pago := 0;
  bieps := 0;

  //OBTIENE LOS DATOS DEL DEPOSITO
  qryCFDI.SQL.Clear;
  {qryCFDI.SQL.Text:='select max(dcc.deposito_cc_id) as deposito_cc_id,max(dcc.fecha) as fecha,cc.folio,max(dcc.refer_movto_bancario) as refer_movto_bancario,'+
                  ' max((select nombre from clientes where cliente_id=cc.cliente_id)) as nombre,max(cc.docto_cc_id) as docto_cc_id,'+
                  ' max(idcc.importe) as importe'+
                  ' from depositos_cc dcc join depositos_cc_det dccd'+
                  ' on dcc.deposito_cc_id=dccd.deposito_cc_id join doctos_cc cc'+
                  ' on cc.docto_cc_id=dccd.docto_cc_id join importes_doctos_cc idcc'+
                  ' on idcc.docto_cc_id=cc.docto_cc_id'+
                  ' /*where dcc.fecha between :F_FINI and :F_FFIN*/'+
                  ' where dcc.deposito_cc_id=:id'+
                  ' group by cc.folio';   }
                    qryCFDI.SQL.Text:='select first 1 dcc.deposito_cc_id,dcc.fecha,cc.folio,dcc.refer_movto_bancario,'+
                      ' (select nombre from clientes where cliente_id=cc.cliente_id),cc.docto_cc_id,'+
                      ' idcc.importe'+
                      ' from depositos_cc dcc join depositos_cc_det dccd'+
                      ' on dcc.deposito_cc_id=dccd.deposito_cc_id join doctos_cc cc'+
                      ' on cc.docto_cc_id=dccd.docto_cc_id join importes_doctos_cc idcc'+
                      ' on idcc.docto_cc_id=cc.docto_cc_id'+
                      ' /*where dcc.fecha between :F_FINI and :F_FFIN*/'+
                      ' where dcc.deposito_cc_id=:id';
  qryCFDI.ParamByName('id').Value:=qryDepositos.FieldByName('deposito_cc_id').Value;
  qryCFDI.Open();
  qryCFDI.First;
  while not qryCFDI.Eof do
  begin
  folio_xml := qryCFDI.FieldByName('folio').AsString;
  folio_deposito:=qryCFDI.FieldByName('folio').AsString;
  folio_deposito:=removeLeadingZeros(folio_deposito);
  id_deposito:=qryCFDI.FieldByName('deposito_cc_id').AsInteger;
  referencia:=qryCFDI.FieldByName('refer_movto_bancario').AsString;
  importe_deposito:=Conexion.ExecSQLScalar('select importe from depositos_cc where deposito_cc_id=:d',[id_deposito]);

   //CREAR ENCABEZADO DE POLIZA
    tipo_poliza_id :=74;
     if Conexion.ExecSQLScalar('select prefijo from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]) = null then
        prefijo_poliza := ''
      else
        prefijo_poliza := Conexion.ExecSQLScalar('select prefijo from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]);

      tipo_cons := Conexion.ExecSQLScalar('select tipo_consec from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]);
      if tipo_cons = 'M' then
      begin
        mes := Monthof(qryCFDI.FieldByName('fecha').AsDateTime);
        anio := YearOf(qryCFDI.FieldByName('fecha').AsDateTime);
        tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                        ' where tipo_poliza_id = :pid and mes=:mes and ano=:a',[tipo_poliza_id,mes,anio]);
        if tipo_poliza_det_id = 0 then
        begin
         tipo_poliza_det_id := Conexion.ExecSQLScalar('insert into tipos_polizas_det values(-1,:pid,:a,:m,1) returning tipo_poliza_det_id',[tipo_poliza_id,anio,mes]);
        end;
      end;
      if tipo_cons = 'E' then
      begin
        anio := YearOf(qryCFDI.FieldByName('fecha').AsDateTime);
        tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                        ' where tipo_poliza_id = :pid and  ano=:a',[tipo_poliza_id,anio]);
      end;
      if tipo_cons = 'P' then
      begin
        tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                        ' where tipo_poliza_id = :pid',[tipo_poliza_id]);
      end;
      consec := Conexion.ExecSQLScalar('select consecutivo from tipos_polizas_det where tipo_poliza_det_id = :tp',[tipo_poliza_det_id]);
      folio_poliza := prefijo_poliza + Format('%.*d',[9-(prefijo_poliza.length), consec]);
      descripcion := Quotedstr('Referencia:'+referencia+sLineBreak+Conexion.ExecSQLScalar('select nombre from repositorio_cfdi where folio=:f',[folio_deposito]));

           query_str := 'insert into doctos_co(docto_co_id,tipo_poliza_id,poliza,fecha,moneda_id,tipo_cambio,estatus,sistema_origen,descripcion,aplicado)'+
                          ' values(-1,'+inttostr(tipo_poliza_id)+','+QuotedStr(folio_poliza)+','''+FormatDateTime('mm/dd/yyyy',qryCFDI.FieldByName('fecha').AsDateTime)+''',1,1,''N'',''CO'','+descripcion+',''N'') returning docto_co_id';
            poliza_id := Conexion.ExecSQLScalar(query_str);

            {**************************ACTUALIZA EL DETALLE DEL TIPO DE POLIZA***************************}
            Conexion.ExecSQL('update tipos_polizas_det set consecutivo = consecutivo+1 where tipo_poliza_det_id = :t',[tipo_poliza_det_id]);

  banco_cfdi_id:=Conexion.ExecSQLScalar('select CTA_BANCOS_GENERAL_ID from SIC_PLANTILLAS_CONFIG');
  CreaDetalle1(poliza_id,banco_cfdi_id,importe_deposito,0,folio_xml);

  qryCobrosDeposito.SQL.Clear;
  qryCobrosDeposito.SQL.Text:='select * from depositos_cc_det where deposito_cc_id=:deposito_cc_id';
  qryCobrosDeposito.ParamByName('deposito_cc_id').Value:=id_deposito;
  qryCobrosDeposito.Open;
  qryCobrosDeposito.First;
    while not qryCobrosDeposito.EoF do
     begin
    id_cobro:=qryCobrosDeposito.FieldByName('docto_cc_id').Value;


    qryCargos.SQL.Text:='select * from cargos_acreditados_cc(:id_cobro)';
    qryCargos.ParamByName('id_cobro').Value:=id_cobro;
    qryCargos.Open();

            //RECORRER LOS CARGOS
        qryCargos.First;
    if qryCargos.RecNo=0 then
      begin
      CreaDetalle(poliza_id,Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where upper(nombre)=''CLIENTES'''),0,Conexion.ExecSQLScalar('select importe from importes_doctos_cc where docto_cc_id=:d',[id_cobro]),folio_cargo);
      qryCobrosDeposito.Next;
      end
      else
      begin

        while not qryCargos.Eof do
          begin
            importe_cargo:=0;
            impuesto_cargo:=0;
            folio_cobro:=Conexion.ExecSQLScalar('select folio from doctos_cc where docto_cc_id=:id',[id_cobro]);
            importe_cobro:=qryCargos.FieldByName('importe_acreditado').AsFloat;
            importe_cobro:=strtofloat(formatfloat('0.0000',importe_cobro));
            id_cobro:=qryCargos.FieldByName('docto_cc_acr_id').AsInteger;


            if folio_cobro[1]='A' then
               begin
               folio_cobro:=folio_cobro.Substring(1,folio_cobro.Length);
               folio_cobro:=removeLeadingZeros(folio_cobro);
               folio_cobro:='A'+folio_cobro;
               end
               else folio_cobro:=removeLeadingZeros(folio_cobro);


            //RECORRE LOS CARGOS
            qryCobros.SQL.Clear;
            qryCobros.SQL.Text:='select * from importes_doctos_cc where docto_cc_id=:docto_cc_id';
            qryCobros.ParamByName('docto_cc_id').Value:=id_cobro;
            qryCobros.Open();
            qryCobros.First;
              while not qryCobros.Eof do
              begin
               folio_cargo:=Conexion.ExecSQLScalar('select folio from doctos_cc where docto_cc_id=:d',[id_cobro]);
               if System.Char.IsLetter(folio_cargo,1) then
               begin
                letra:=folio_cargo[1];
               folio_cargo:=folio_cargo.Substring(1,folio_cargo.Length);
               folio_cargo:=removeLeadingZeros(folio_cargo);
               folio_cargo:=letra+folio_cargo;
               end
               else folio_cargo:=removeLeadingZeros(folio_cargo);

                xml_id:=Conexion.ExecSqlScalar('select cfdi_id from repositorio_cfdi where folio=:f',[folio_cargo]);
                {************************LIGA EL CFDI DE LA FACTURA A LA POLIZA RECIEN CREADA****************}
                if xml_id<>0 then
                  begin
                  co_cfd_id := Conexion.ExecSQLScalar('insert into doctos_co_cfdi values(-1,:p,:qr,''N'') returning docto_co_cfdi_id',[poliza_id,xml_id]);
                  Conexion.Commit;
                  end
                  else
                  begin
                  co_cfd_id:=0;
                  end;

              qryXML.SQL.Text:='select cfdi_id from repositorio_cfdi where folio=:folio';
              qryXML.ParamByName('folio').Value:=folio_cargo;
              qryXML.Open();

                importe_cargo:=qryCobros.FieldbyName('importe').AsFloat;


                 //CARGAR XML
              xml:=Conexion.ExecSQLScalar('select xml from repositorio_cfdi where folio=:f',[folio_cargo]);
                 xmlnuevo:=xml.Replace('ü','u');
                  doc := LoadXMLData(xmlnuevo);
               comprobante := doc.ChildNodes['cfdi:Comprobante'];
                //RECORRIDO DE CONCEPTOS
                conceptos := comprobante.ChildNodes['cfdi:Impuestos'].ChildNodes;
                for j := 0 to conceptos.Count -1 do
                begin
                  concepto := Conceptos.Nodes[j];
                  bieps_concepto := 0;
                  c0 := 0;
                  c16 := 0;
                  c8 := 0;
                  if concepto.Attributes['Descuento'] = Null then
                    desc_c := 0
                  else
                    desc_c := strtofloat(concepto.Attributes['Descuento']);
                    // RECORRIDO DE IMPUESTOS
                    for i := 0 to concepto.ChildNodes.Count - 1 do
                    begin
                        imp := concepto.ChildNodes.Nodes[i];
                      // SI ES IEPS
                      if imp.Attributes['Impuesto'] = '003' then
                      begin
                       tasa_ieps:=strtofloat(imp.Attributes['TasaOCuota']);
                       //impuesto_cargo:=qryCobros.FieldbyName('impuesto').AsFloat;
                       impuesto_cargo:=strtofloat(imp.Attributes['Importe']);
                       importe_cargo_total:=strtofloat(formatfloat('0.0000',importe_cargo))+strtofloat(formatfloat('0.0000',impuesto_cargo));
                       impuesto_proporcional:=(importe_cobro*impuesto_cargo)/(importe_cargo_total);
                       if impuesto_cargo>0 then
                        begin
                        importe_cobro:=importe_cobro-impuesto_proporcional;
                        end;
                                                  //COMPARACION DE IMPORTES
                       if importe_cargo_total<>importe_cobro then
                          begin
                          if impuesto_proporcional<>0 then
                           begin
                           // impuesto_proporcional:=(importe_cobro*impuesto_cargo)/(importe_cargo_total);
                            ////////////////////////////////INSERTAR DETALLE POLIZA//////////////////////////////
                           CreaDetalle(poliza_id,Conexion.ExecSQLScalar('select cta_ieps_id from sic_cuentas_co_gen_clientes'),0,impuesto_proporcional,folio_cargo);
                           Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_id]);
                           end;
                          end;

                      end
                      // SI ES IVA
                      else if imp.Attributes['Impuesto'] = '002' then
                      begin
                        tasa_iva:=strtofloat(imp.Attributes['TasaOCuota']);
                       impuesto_cargo:=strtofloat(imp.Attributes['Importe']);
                       importe_cargo_total:=strtofloat(formatfloat('0.0000',importe_cargo))+strtofloat(formatfloat('0.0000',impuesto_cargo));
                       impuesto_proporcional:=(importe_cobro*impuesto_cargo)/(importe_cargo_total);
                              if impuesto_cargo>0 then
                        begin
                        importe_cobro:=importe_cobro-impuesto_proporcional;
                        end;
                                   //COMPARACION DE IMPORTES
                       if importe_cargo_total<>importe_cobro then
                          begin
                          if impuesto_proporcional<>0 then
                           begin
                           // impuesto_proporcional:=(importe_cobro*impuesto_cargo)/(importe_cargo_total);
                            ////////////////////////////////INSERTAR DETALLE POLIZA//////////////////////////////
                           CreaDetalle(poliza_id,Conexion.ExecSQLScalar('select cta_iva_pagado_id from sic_cuentas_co_gen_clientes'),0,impuesto_proporcional,folio_cargo);
                           Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_id]);
                           end;
                          end;
                      end;


                    end;
                end;
                     //INSERTA CUENTA CLIENTES
               CreaDetalle(poliza_id,Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where upper(nombre)=''CLIENTES'''),0,importe_cobro,folio_cargo);
                qryCobros.Next;
              end;

            //SIGUIENTE CARGO
            qryCargos.Next;
          end;
        qryCobrosDeposito.Next;
        end;
     end;
  qryCFDI.Next;
  creadas := creadas + 1;
  Conexion.ExecSQL('insert into SIC_DEPOSITOS_POLIZAS(DEP_POL_ID,DEPOSITO_CC_ID,DOCTO_CO_ID) values(-1,:id1,:id2)',[id_deposito,poliza_id]);
  end;

end;

procedure TFPrincipal.Plantillas1Click(Sender: TObject);
var
  pl : TFPlantillas;
begin
  pl := TFPlantillas.Create(nil);
  pl.Conexion.Params := Conexion.Params;
  pl.Conexion.Open;

  pl.qryTiposPolizasContado.Open;
  pl.qryPlantillasContado.Open;
  pl.qryTiposPolizasCredito.Open;
  pl.qryTiposPolizasContadoP.Open;
  pl.qryTiposPolizasCreditoP.Open;
  pl.qrytiposPolizasPagosClientes.Open;
  pl.qryTiposPolizasPagosPro.Open;
  pl.qryTiposPolizasNomina.open;
  pl.qryTiposPolizasNCCXC.Open;
  pl.qryTiposPolizasNCCXP.Open;

  pl.qryPlantillasCredito.Open;
  pl.qryPlantillasContadoCompras.Open;
  pl.qryPlantillasCreditoCompras.Open;
  pl.qryPlantillapagosaclientes.open;
  pl.qryPlantillapagosaproveedores.open;
  pl.qryPlantillanomina.Open;
  pl.qryPlantillaNCCCXC.Open;
  pl.qryPlantillaNCCXP.Open;

  pl.Act := False;
  pl.ShowModal;
end;

procedure TFPrincipal.Preferencias1Click(Sender: TObject);
var
  pl : TFConfig;
begin
  pl := TFConfig.Create(nil);
  pl.Conexion.Params := Conexion.Params;
  pl.Conexion.Open;
  pl.qryClientes.Open;
  pl.qryProveedores.Open;
  pl.ShowModal;
end;

procedure TFPrincipal.Proveedores1Click(Sender: TObject);
var
  ctes : TFProveedores;
begin
  ctes := TFProveedores.Create(nil);
  ctes.Conexion.Params := Conexion.Params;
  ctes.Conexion.Open;
  ctes.qryProveedores.Open;
  ctes.ShowModal;
end;

procedure TFPrincipal.qryDepositosAfterOpen(DataSet: TDataSet);
begin
   with grdCFDI do
  begin
    //deposito_id
    Columns[0].Visible := False;
    //fecha
    Columns[1].Width := 100;
    Columns[1].Title.Caption := 'Fecha';
    //folio
    Columns[2].Width := 250;
    Columns[2].Title.Caption := 'Cuenta Bancaria';
    //Polizas
    Columns[3].Width := 430;
    Columns[3].Title.Caption := 'Polizas';
    //nombre
    Columns[4].Width := 100;
    Columns[4].Title.Caption := 'Referencia';
    //importe
    Columns[5].Width := 80;
    Columns[5].Title.Caption := 'Importe';
    TFloatField(Fields[5]).DisplayFormat := '###,##0.00';
  end;
end;

procedure TFPrincipal.txtBusquedaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    actualizarvista;
end;

function TFPrincipal.filtros : string;
var
  filtros:string;
begin

end;

procedure TFPrincipal.Actualizarvista;
begin
  //
  qryDepositos.ParamByName('F_FINI').AsDate := dtpInicio.Date;
  qryDepositos.ParamByName('F_FFIN').AsDate := dtpFin.Date;
  qryDepositos.Refresh;
  qryDepositos.Filter := filtros + 'upper(refer_movto_bancario) like ''%'+txtBusqueda.Text+'%''';
  qryDepositos.First;
  qryDepositos.FetchAll;
  //qryCFDI.ParamByName('F_FINI').AsDate := dtpInicio.Date;
 // qryCFDI.ParamByName('F_FFIN').AsDate := dtpFin.Date;
 // qryCfdi.Refresh;
 // qryCFDI.Filter := filtros + ' and (upper(folio) like ''%'+txtBusqueda.Text+'%'' or upper(nombre) like ''%'+txtBusqueda.Text+'%'' or upper(rfc) like ''%'+txtBusqueda.Text+'%'' )';
 //qryCFDI.Filter := filtros + 'upper(folio) like ''%'+txtBusqueda.Text+'%''';
 // qryCfdi.First;
 // qryCFDI.FetchAll;
  lblNumComprobantes.Caption := inttostr(qryDepositos.RecordCount)+' Comprobantes';
end;

procedure TFPrincipal.btnBuscarClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.btnGenerarTodasClick(Sender: TObject);
var
  ch,i : integer;
begin
    creartodas;
    actualizarvista;
end;

procedure TFPrincipal.CrearTodaspordia;
begin
{  AssignFile(log, 'log.txt');
  Rewrite(log);
  WriteLn(log,formatdatetime('dd/mm/yyyy t',Now)); }
  qryDepositos.First;
  try
    while not qryDepositos.eof do
    begin
      Generapoliza;
        try
          Conexion.ExecSQL('update doctos_co set aplicado=''S'' where docto_co_id = :pid',[poliza_id]);
          Conexion.ExecSQL('execute procedure aplica_docto_co(:docto_id,''A'')',[poliza_id]);
          Conexion.Commit;
        except
          Conexion.ExecSQL('update doctos_co set estatus = ''P'' where docto_co_id = :pid',[poliza_id]);
      end;
       qryDepositos.Next;
    end;
  except
  //  CloseFile(log);
  end;
 // CloseFile(log);
  ShowMessage(IntToStr(creadas)+' Comprobantes procesados correctamente');
  creadas := 0;
  poliza_id := 0;
end;

procedure TFPrincipal.CrearTodas;
begin
 { AssignFile(log, 'log.txt');
  Rewrite(log);
  WriteLn(log,formatdatetime('dd/mm/yyyy t',Now));   }

  ProgressBar1.Max := qryDepositos.RecordCount;
ProgressBar1.Step := 1;
ProgressBar1.Min := 0;
ProgressBar1.Position := 0;
  qryDepositos.First;

  try
    while not qryDepositos.eof do
    begin
      Generapoliza;
        try
        Conexion.ExecSQL('update doctos_co set aplicado=''S'' where docto_co_id = :pid',[poliza_id]);
        Conexion.ExecSQL('execute procedure aplica_docto_co(:docto_id,''A'')',[poliza_id]);
        Conexion.Commit;
        except
        Conexion.ExecSQL('update doctos_co set estatus = ''P'' where docto_co_id = :di',[poliza_id]);
       end;
         ProgressBar1.StepIt;
         ProgressBar1.Update;
      qryDepositos.Next;
    end;
  {  if  (cbCreaPolizas.ItemIndex = 1) then
    begin }
   // end;
  except
  //  CloseFile(log);
  end;
 // CloseFile(log);
  ShowMessage(IntToStr(creadas)+' Comprobantes procesados correctamente');
  creadas := 0;
  poliza_id := 0;
end;

procedure TFPrincipal.cbComprasClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.cbCreaPolizasChange(Sender: TObject);
begin
//  case cbCreaPolizas.ItemIndex of
//    0:begin
//        label3.Visible := True;
//        dtpFin.Visible := True;
//      end;
//    1:begin
//        label3.Visible := False;
//        dtpFin.Visible := False;
//        dtpFin.Date := dtpInicio.Date;
//        actualizarvista;
//      end;
//  end;

end;

procedure TFPrincipal.cbNominaClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.cbNotasCreditoCXCClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.cbNotasCreditoCxpClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.cbPagoProveedoresClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.cbPagosClientesClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.cbTipoChange(Sender: TObject);

begin
  qryCFDI.Filter := 'upper(folio) like ''%'+txtBusqueda.Text+'%'' or upper(nombre) like ''%'+txtBusqueda.Text+'%'' or upper(rfc) like ''%'+txtBusqueda.Text+'%'' ';
  qryCfdi.FetchAll;
  lblNumComprobantes.Caption := inttostr(qryCfdi.RecordCount)+' Comprobantes';
end;

procedure TFPrincipal.cbVentasClick(Sender: TObject);
begin
  actualizarvista;
end;

procedure TFPrincipal.Clientes1Click(Sender: TObject);
var
  ctes : TFClientes;
begin
  ctes := TFclientes.Create(nil);
  ctes.Conexion.Params := Conexion.Params;
  ctes.Conexion.Open;
  ctes.qryClientes.Open;
  ctes.ShowModal;
end;

procedure TFPrincipal.CreaTablas;
var
  tabla,tipo_poliza_id,tipo_poliza_compra_id,cta_bancos_id : integer;
begin
  //TABLA DE CUENTAS CONTABLES DE LOS CLIENTES
  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_CUENTAS_CONTABLES_CLIENTES'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_CUENTAS_CONTABLES_CLIENTES ('+
    ' CLIENTE_ID               INTEGER,'+
    ' CTA_VENTAS0_CONTADO_ID   INTEGER,'+
    ' CTA_VENTAS0_CREDITO_ID   INTEGER,'+
    ' CTA_VENTAS16_CONTADO_ID  INTEGER,'+
    ' CTA_VENTAS16_CREDITO_ID  INTEGER,'+
    ' CTA_VENTAS8_CONTADO_ID   INTEGER,'+
    ' CTA_VENTAS8_CREDITO_ID   INTEGER,'+
    ' CTA_IVA_PENDIENTE_ID     INTEGER,'+
    ' CTA_IVA_PAGADO_ID        INTEGER,'+
    ' CTA_DESCUENTO_ID         INTEGER,'+
    ' CTA_IEPS_ID              INTEGER'+
    ' );');

    Conexion.ExecSQL('grant all on SIC_CUENTAS_CONTABLES_CLIENTES to usuario_microsip');
  end;

  //TABLA DE CUENTAS CONTABLES DE LOS PROVEEDORES
  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_CUENTAS_CO_PROVEEDORES'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_CUENTAS_CO_PROVEEDORES ('+
    ' PROVEEDOR_ID              INTEGER,'+
    ' CTA_COMPRAS0_CONTADO_ID   INTEGER,'+
    ' CTA_COMPRAS0_CREDITO_ID   INTEGER,'+
    ' CTA_COMPRAS16_CONTADO_ID  INTEGER,'+
    ' CTA_COMPRAS16_CREDITO_ID  INTEGER,'+
    ' CTA_COMPRAS8_CONTADO_ID   INTEGER,'+
    ' CTA_COMPRAS8_CREDITO_ID   INTEGER,'+
    ' CTA_IVA_PENDIENTE_ID      INTEGER,'+
    ' CTA_IVA_PAGADO_ID         INTEGER,'+
    ' CTA_DESCUENTO_ID          INTEGER,'+
    ' CTA_IEPS_ID               INTEGER,'+
    ' IGNORAR                   VARCHAR(1)'+
    ' );');

    Conexion.ExecSQL('grant all on SIC_CUENTAS_CO_PROVEEDORES to usuario_microsip');
  end;

  //TABLA DE DETALLADO DE PLANTILLAS DE VENTAS
  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_PLANTILLAS_CO_DET'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_PLANTILLAS_CO_DET ('+
    ' CLASIF    VARCHAR(20),'+
    ' TIPO      CHAR(1),'+
    ' CTA       VARCHAR(50),'+
    ' DEBE      VARCHAR(50),'+
    ' HABER     VARCHAR(50),'+
    ' FORMULA_DEBE     VARCHAR(50),'+
    ' FORMULA_HABER     VARCHAR(50),'+
    ' CREACION  TIMESTAMP'+
    ' );');
    Conexion.ExecSQL('grant all on SIC_PLANTILLAS_CO_DET to usuario_microsip');
  end;
  tabla := Conexion.ExecSQLScalar('select count(*) from sic_plantillas_co_det  ');
  if tabla = 0 then
  begin
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''R'',''Cuenta Clientes'',''Total'',''0.00'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''R'',''IVA Pendiente'',''0.00'',''IVA 16%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''R'',''IEPS'',''0.00'',''IEPS'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''C'',''Cuenta Bancos General'',''Total'',''0.00'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''C'',''IVA Pagado'',''0.00'',''IVA 16%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''R'',''Ventas 16% credito'',''0.00'',''Base 16%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''R'',''Ventas 0% credito'',''0.00'',''Base 0%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''R'',''Ventas 8% credito'',''0.00'',''Base 8%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''R'',''Descuento'',''Descuento'',''0.00'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''C'',''Ventas 0% contado'',''0.00'',''Base 0%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''C'',''Ventas 16% contado'',''0.00'',''Base 16%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''C'',''Ventas 8% contado'',''0.00'',''Base 8%'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Venta'',''C'',''Descuento'',''Descuento'',''0.00'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''R'', ''Compras 0% credito'', ''Base 0%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''R'', ''Compras 16% credito'', ''Base 16%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''R'', ''IVA Pendiente'', ''IVA 16%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''R'', ''Cuenta proveedores'', ''0.00'', ''Total'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''C'', ''Compras 0% contado'', ''Base 0%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''C'', ''Compras 16% contado'', ''Base 16%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''C'', ''Compras 8% contado'', ''Base 8%'', ''0.00'',current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''C'', ''IVA Pendiente'', ''IVA 16%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Compra'', ''C'', ''Cuenta Bancos General'', ''0.00'', ''Total'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Pago a proveedor'', NULL, ''Cuenta proveedores'', ''Total'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Pago a proveedor'', NULL, ''Cuenta Bancos General'', ''0.00'', ''Total'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Pago de cliente'', NULL, ''Cuenta Bancos General'', ''Total'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Pago de cliente'', NULL, ''Cuenta Clientes'', ''0.00'', ''Total'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Nota de credito cxc'', NULL, ''Ventas 16% contado'', ''Base 16%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Nota de credito cxc'', NULL, ''IVA Pagado'', ''IVA 16%'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Nota de credito cxc'', NULL, ''Cuenta Bancos General'', ''0.00'', ''Total'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Nota de credito cxp'', NULL, ''Cuenta Bancos General'', ''Total'', ''0.00'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Nota de credito cxp'', NULL, ''Compras 16% contado'', ''0.00'', ''Base 16%'', current_timestamp);');
    Conexion.Execsql('INSERT INTO SIC_PLANTILLAS_CO_DET (CLASIF, TIPO, CTA, DEBE, HABER, CREACION) VALUES (''Nota de credito cxp'', NULL, ''IVA Pagado'', ''0.00'', ''IVA 16%'', current_timestamp);');

  end;

  //TABLA DE CONFIGURACION DE PLANTILLAS
  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_PLANTILLAS_CONFIG'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_PLANTILLAS_CONFIG ('+
    ' ID                              INTEGER NOT NULL,'+
    ' CTA_BANCOS_GENERAL_ID           INTEGER,'+
    ' TIPO_POLIZA_CONTADO_ID          INTEGER,'+
    ' TIPO_POLIZA_CREDITO_ID          INTEGER,'+
    ' TIPO_POLIZA_COMPRAS_CONTADO_ID  INTEGER,'+
    ' TIPO_POLIZA_COMPRAS_CREDITO_ID  INTEGER,'+
    ' TIPO_POLIZA_PAGOCXC_ID          INTEGER,'+
    ' TIPO_POLIZA_PAGOCXP_ID          INTEGER,'+
    ' TIPO_POLIZA_NOMINA_ID           INTEGER,'+
    ' TIPO_POLIZA_NCCXC_ID            INTEGER,'+
    ' TIPO_POLIZA_NCCXP_ID            INTEGER'+
    ' );');
    Conexion.ExecSQL('grant all on SIC_PLANTILLAS_CONFIG to usuario_microsip');
  end;
  if conexion.ExecSQLScalar('select count(*) from sic_plantillas_config')=0 then
  begin
    tipo_poliza_id := Conexion.ExecSQLScalar('select tipo_poliza_id from tipos_polizas where upper(nombre) =''INGRESOS''');
    tipo_poliza_compra_id :=  Conexion.ExecSQLScalar('select tipo_poliza_id from tipos_polizas where upper(nombre) =''EGRESOS''');
    cta_bancos_id := Conexion.ExecSQLScalar('select first 1 cuenta_id from cuentas_co where nombre like ''BANC%'' ');
    Conexion.ExecSQL('insert into sic_plantillas_config values(1,'+inttostr(cta_bancos_id)+','+inttostr(tipo_poliza_id)+
                    ','+inttostr(tipo_poliza_id)+','+inttostr(tipo_poliza_compra_id)+','+inttostr(tipo_poliza_compra_id)+',null,null,null,null,null);');
  end;


  //CUENTAS GENERALES DE CLIENTES
  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_CUENTAS_CO_GEN_CLIENTES'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_CUENTAS_CO_GEN_CLIENTES ('+
    ' CUENTA_CLIENTES_ID   INTEGER,'+
    ' CTA_VENTAS0_CONTADO_ID   INTEGER,'+
    ' CTA_VENTAS0_CREDITO_ID   INTEGER,'+
    ' CTA_VENTAS16_CONTADO_ID  INTEGER,'+
    ' CTA_VENTAS16_CREDITO_ID  INTEGER,'+
    ' CTA_VENTAS8_CONTADO_ID   INTEGER,'+
    ' CTA_VENTAS8_CREDITO_ID   INTEGER,'+
    ' CTA_IVA_PENDIENTE_ID     INTEGER,'+
    ' CTA_IVA_PAGADO_ID        INTEGER,'+
    ' CTA_DESCUENTO_ID         INTEGER,'+
    ' CTA_IEPS_ID              INTEGER'+
    ' );');
    Conexion.ExecSQL('grant all on SIC_CUENTAS_CO_GEN_CLIENTES to usuario_microsip');
    Conexion.ExecSQL('insert into SIC_CUENTAS_CO_GEN_CLIENTES values(null,null,null,null,null,null,null,null,null,null,null)');
  end;

  //CUENTAS GENERALES DE PROVEEDORES
  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_CUENTAS_CO_GEN_PROVEEDORES'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_CUENTAS_CO_GEN_PROVEEDORES ('+
    ' CTA_PROVEEDORES_ID     INTEGER,'+
    ' CTA_COMPRAS0_CONTADO_ID   INTEGER,'+
    ' CTA_COMPRAS0_CREDITO_ID   INTEGER,'+
    ' CTA_COMPRAS16_CONTADO_ID  INTEGER,'+
    ' CTA_COMPRAS16_CREDITO_ID  INTEGER,'+
    ' CTA_COMPRAS8_CONTADO_ID   INTEGER,'+
    ' CTA_COMPRAS8_CREDITO_ID   INTEGER,'+
    ' CTA_IVA_PENDIENTE_ID      INTEGER,'+
    ' CTA_IVA_PAGADO_ID         INTEGER,'+
    ' CTA_DESCUENTO_ID          INTEGER,'+
    ' CTA_IEPS_ID               INTEGER'+
    ' );');
    Conexion.ExecSQL('grant all on SIC_CUENTAS_CO_GEN_PROVEEDORES to usuario_microsip');
    Conexion.ExecSQL('insert into SIC_CUENTAS_CO_GEN_PROVEEDORES values(null,null,null,null,null,null,null,null,null,null,null)');
  end;

    //TABLA SIC_DEPOSITOS_POLIZAS
  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_DEPOSITOS_POLIZAS'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_DEPOSITOS_POLIZAS ('+
    ' DEP_POL_ID   INTEGER,'+
    ' DEPOSITO_CC_ID   INTEGER,'+
    ' DOCTO_CO_ID   INTEGER'+
    ' );');
    Conexion.ExecSQL('grant all on SIC_DEPOSITOS_POLIZAS to usuario_microsip');
    Conexion.ExecSQL('CREATE SEQUENCE SIC_DEP_POL_ID START WITH 0 INCREMENT BY 1;');
    Conexion.ExecSQL('ALTER SEQUENCE SIC_DEP_POL_ID RESTART WITH 0;');
    Conexion.ExecSQL('CREATE OR ALTER TRIGGER SIC_DEPOSITOS_POLIZAS_BI0 FOR SIC_DEPOSITOS_POLIZAS'+
                      ' ACTIVE BEFORE INSERT POSITION 0'+
    ' AS begin /* Genera un nuevo id */ if (new.dep_pol_id = -1) then new.dep_pol_id = gen_id(SIC_DEP_POL_ID,1); end');
  end;
end;

procedure TFPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TFPrincipal.FormCreate(Sender: TObject);
begin
  nf := 0;
end;

{procedure TFPrincipal.CreateParams(var params: TCreateParams);
begin
  inherited Createparams(Params);
  Params.ExStyle := Params.ExStyle or WS_ex_APPWINDOW;
  params.WndParent := GetDesktopWindow;
end;}

procedure TFPrincipal.FormShow(Sender: TObject);
begin
  CreaTablas;
  dtpInicio.Date := EncodeDate(YearOf(Now),MonthOf(Now),1);
  dtpFin.Date := Now;
    qryDepositos.ParamByName('F_FINI').AsDate := dtpInicio.Date;
  qryDepositos.ParamByName('F_FFIN').AsDate := dtpFin.Date;
  qryDepositos.Open;
  qryDepositos.FetchAll;
 // qryCFDI.ParamByName('F_FINI').AsDate := dtpInicio.Date;
 // qryCFDI.ParamByName('F_FFIN').AsDate := dtpFin.Date;
 // qryCFDI.Open;


  //qrycfdi.FetchAll;
  lblNumComprobantes.Caption := inttostr(qryCfdi.RecordCount)+' Combprobantes';
  actualizarvista;
  qryPlantilla.Open;
end;

function TFPrincipal.removeLeadingZeros(const Value: string): string;
var
  i: Integer;
begin
  for i := 1 to Length(Value) do
    if Value[i]<>'0' then
    begin
      Result := Copy(Value, i, MaxInt);
      exit;
    end;
  Result := '';
end;

end.
