unit UPreferencias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, UCuentasCo,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.ComCtrls;

type
  TFConfig = class(TForm)
    Label1: TLabel;
    txtCtaBancos: TEdit;
    btnGuardar: TButton;
    btnSalir: TButton;
    Conexion: TFDConnection;
    qryClientes: TFDQuery;
    qryProveedores: TFDQuery;
    gbIVAClientes: TGroupBox;
    Label5: TLabel;
    Label11: TLabel;
    txtIVAPendiente: TEdit;
    txtIVAPagado: TEdit;
    procedure txtCtaBancosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnGuardarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtIVAPagadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure btnAsignarANuevosClick(Sender: TObject);
    procedure btnAsignarATodosClick(Sender: TObject);
    procedure CargarCuentas;
    procedure txtCtaClientesExit(Sender: TObject);
    procedure txtDescuentoExit(Sender: TObject);
    procedure txtVentas0creditoExit(Sender: TObject);
    procedure txtVentas0ContadoExit(Sender: TObject);
    procedure txtVentas16CreditoExit(Sender: TObject);
    procedure txtVentas16ContadoExit(Sender: TObject);
    procedure txtVentas8CreditoExit(Sender: TObject);
    procedure txtVentas8ContadoExit(Sender: TObject);
    procedure txtIVAPendienteExit(Sender: TObject);
    procedure txtIVAPagadoExit(Sender: TObject);
    procedure txtCtaIEPSExit(Sender: TObject);
    procedure txtCtaProveedoresExit(Sender: TObject);
    procedure txtDescuentoProveedoresExit(Sender: TObject);

    procedure txtCompras0CreditoExit(Sender: TObject);
    procedure txtCompras16CreditoExit(Sender: TObject);
    procedure txtCompras0ContadoExit(Sender: TObject);
    procedure txtCompras16ContadoExit(Sender: TObject);
    procedure txtCompras8CreditoExit(Sender: TObject);
    procedure txtCompras8ContadoExit(Sender: TObject);
    procedure txtIVAPendienteProveedoresExit(Sender: TObject);
    procedure txtIVAPagadoProveedoresExit(Sender: TObject);
    procedure txtIEPSProveedoresExit(Sender: TObject);

    procedure btnAsignaraProveedoresNuevosClick(Sender: TObject);
    procedure btnAsignaraTodosProveedoresClick(Sender: TObject);
    procedure txtIVAPendienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConfig: TFConfig;

implementation

{$R *.dfm}

procedure TFConfig.cargarcuentas;
begin

  txtIVAPendiente.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_iva_pendiente_id from sic_cuentas_co_gen_clientes)');
  txtIVAPagado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_iva_pagado_id from sic_cuentas_co_gen_clientes)');

end;

procedure TFConfig.btnAsignarANuevosClick(Sender: TObject);
var
  cliente_id : integer;
  cta_ventas0_contado_id, cta_ventas0_credito_id,
  cta_ventas16_contado_id, cta_ventas16_credito_id,
  cta_ventas8_contado_id, cta_ventas8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_ctes_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  qryClientes.First;
  while not qryClientes.eof do
  begin
    cambios := 0;
    cliente_id := qryclientes.FieldByName('cliente_id').AsInteger;
    if Conexion.ExecSQLScalar('select count(*) from SIC_CUENTAS_CONTABLES_CLIENTES where cliente_id ='+inttostr(cliente_id))=0 then
    begin

      cta_iva_pendiente_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPendiente.Text]);
      cta_iva_pagado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPagado.Text]);
       Conexion.ExecSQL('insert into sic_cuentas_contables_clientes values('+
          ':cliente_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps)',
          [cliente_id,
          cta_ventas0_contado_id,cta_ventas0_credito_id,
          cta_ventas16_contado_id,cta_ventas16_credito_id,
          cta_ventas8_contado_id,cta_ventas8_credito_id,
          cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id]);
       
       cambios := cambios + 1;
    end;
    qryclientes.Next;
  end;
  showmessage('Proceso terminado ' + inttostr(cambios)+' Clientes modificados');
end;



procedure TFConfig.btnAsignaraProveedoresNuevosClick(Sender: TObject);
var
  proveedor_id : integer;
  cta_compras0_contado_id, cta_compras0_credito_id,
  cta_compras16_contado_id, cta_compras16_credito_id,
  cta_compras8_contado_id, cta_compras8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_proveedores_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  qryProveedores.First;
  while not qryProveedores.eof do
  begin
    cambios := 0;
    proveedor_id := qryproveedores.FieldByName('proveedor_id').AsInteger;
    if Conexion.ExecSQLScalar('select count(*) from SIC_CUENTAS_CO_PROVEEDORES where proveedor_id ='+inttostr(proveedor_id))=0 then
    begin
   Conexion.ExecSQL('insert into sic_cuentas_co_proveedores values('+
          ':proveedor_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps,:IGN)',
          [proveedor_id,
          cta_compras0_contado_id,cta_compras0_credito_id,
          cta_compras16_contado_id,cta_compras16_credito_id,
          cta_compras8_contado_id,cta_compras8_credito_id,
          cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id,'N']);
       
       cambios := cambios + 1;
    end;
    qryProveedores.Next;
  end;
  showmessage('Proceso terminado ' + inttostr(cambios)+' Proveedores modificados');
end;
procedure TFConfig.btnAsignarATodosClick(Sender: TObject);
var
  cliente_id : integer;
  cta_ventas0_contado_id, cta_ventas0_credito_id,
  cta_ventas16_contado_id, cta_ventas16_credito_id,
  cta_ventas8_contado_id, cta_ventas8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_ctes_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  cambios := 0;
  if MessageDlg('Esta seguro de sobreescribir todas las cuentas ya existentes?',mtConfirmation,mbYesNo,0) = mrYes then
  begin
    qryClientes.First;
    while not qryClientes.eof do
    begin
      cliente_id := qryclientes.FieldByName('cliente_id').AsInteger;

      cta_iva_pendiente_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPendiente.Text]);
      cta_iva_pagado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPagado.Text]);

      if Conexion.ExecSQLScalar('select count(*) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) > 0 then
      begin
        Conexion.ExecSQL('update sic_cuentas_contables_clientes set'+
        ' cta_ventas0_contado_id= :cv0c, cta_ventas0_credito_id=:cv0cr,'+
        ' cta_ventas16_contado_id= :cv16c, cta_ventas16_credito_id=:cv16cr,'+
        ' cta_ventas8_contado_id= :cv8c, cta_ventas8_credito_id=:cv8cr,'+
        ' cta_iva_pendiente_id=:cipd,cta_iva_pagado_id=:cip,'+
        ' cta_descuento_id = :cdsid, cta_ieps_id = :cieps'+
        ' where cliente_id = :cid',
        [cta_ventas0_contado_id,cta_ventas0_credito_id,
         cta_ventas16_contado_id,cta_ventas16_credito_id,
         cta_ventas8_contado_id,cta_ventas8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id, cliente_id])
      end
      else
      begin
        Conexion.ExecSQL('insert into sic_cuentas_contables_clientes values('+
        ':cliente_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps)',
        [cliente_id,
        cta_ventas0_contado_id,cta_ventas0_credito_id,
        cta_ventas16_contado_id,cta_ventas16_credito_id,
        cta_ventas8_contado_id,cta_ventas8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id])
      end;
      
      cambios := cambios + 1;
      qryclientes.Next;
    end;
    showmessage('Proceso terminado ' + inttostr(cambios)+' Clientes modificados');
  end;
end;

procedure TFConfig.btnAsignaraTodosProveedoresClick(Sender: TObject);
var
  proveedor_id : integer;
  cta_compras0_contado_id, cta_compras0_credito_id,
  cta_compras16_contado_id, cta_compras16_credito_id,
  cta_compras8_contado_id, cta_compras8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_proveedores_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  cambios := 0;
  if MessageDlg('Esta seguro de sobreescribir todas las cuentas ya existentes?',mtConfirmation,mbYesNo,0) = mrYes then
  begin
    qryProveedores.First;
    while not qryProveedores.eof do
    begin
      proveedor_id := qryProveedores.FieldByName('proveedor_id').AsInteger;


      if Conexion.ExecSQLScalar('select count(*) from sic_cuentas_co_proveedores where proveedor_id = :cid',[proveedor_id]) > 0 then
      begin
        Conexion.ExecSQL('update sic_cuentas_co_proveedores set'+
        ' cta_compras0_contado_id= :cv0c, cta_compras0_credito_id=:cv0cr,'+
        ' cta_compras16_contado_id= :cv16c, cta_compras16_credito_id=:cv16cr,'+
        ' cta_compras8_contado_id= :cv8c, cta_compras8_credito_id=:cv8cr,'+
        ' cta_iva_pendiente_id=:cipd,cta_iva_pagado_id=:cip,'+
        ' cta_descuento_id = :cdsid, cta_ieps_id = :cieps'+
        ' where proveedor_id = :cid',
        [cta_compras0_contado_id,cta_compras0_credito_id,
         cta_compras16_contado_id,cta_compras16_credito_id,
         cta_compras8_contado_id,cta_compras8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id, proveedor_id])
      end
      else
      begin
        Conexion.ExecSQL('insert into sic_cuentas_co_proveedores values('+
        ':cliente_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps,:IGN)',
        [proveedor_id,
        cta_compras0_contado_id,cta_compras0_credito_id,
        cta_compras16_contado_id,cta_compras16_credito_id,
        cta_compras8_contado_id,cta_compras8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id,'N'])
      end;
      
      cambios := cambios + 1;
      qryProveedores.Next;
    end;
    showmessage('Proceso terminado ' + inttostr(cambios)+' Proveedores modificados');
  end;
end;

procedure TFConfig.btnGuardarClick(Sender: TObject);
var
  cta_bancos_id : integer;
  cm : string;
begin
  cta_bancos_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaBancos.Text]);
  cm := 'update sic_plantillas_config set cta_bancos_general_id = '+inttostr(cta_bancos_id);
  Conexion.ExecSQL(cm);
  ModalResult := mrOk;
end;

procedure TFConfig.FormShow(Sender: TObject);
begin
  txtCtaBancos.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_bancos_general_id from sic_plantillas_config)');
  CargarCuentas;
end;

procedure TFConfig.txtCompras0ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras0_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;


procedure TFConfig.txtCompras0CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras0_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;


procedure TFConfig.txtCompras16ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras16_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;


procedure TFConfig.txtCompras16CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras16_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtCompras8ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras8_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtCompras8CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras8_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;


procedure TFConfig.txtCtaBancosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.Open;
    busca_cuentas.txtBuscar.Text :=  txtCtaBancos.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCtaBancos.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
    end;
  end;
end;
procedure TFConfig.txtCtaClientesExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cuenta_clientes_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;

end;



procedure TFConfig.txtCtaIEPSExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ieps_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtCtaProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_proveedores_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtDescuentoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_descuento_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtDescuentoProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_descuento_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtIEPSProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_ieps_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtIVAPagadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtIVAPagadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtIVAPagado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtIVAPagado.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtIVAPagado.Text+''')');;
    end;
  end;
end;

procedure TFConfig.txtIVAPagadoProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;


procedure TFConfig.txtIVAPendienteExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pendiente_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;


procedure TFConfig.txtIVAPendienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtIVAPendiente.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtIVAPendiente.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtIVAPagado.Text+''')');;
    end;
  end;
end;

procedure TFConfig.txtIVAPendienteProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_iva_pendiente_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtVentas0ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas0_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtVentas0creditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas0_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtVentas16ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas16_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtVentas16CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas16_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtVentas8ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas8_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



procedure TFConfig.txtVentas8CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas8_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;



end.
