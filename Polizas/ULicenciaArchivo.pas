unit ULicenciaArchivo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,ULicencia, Vcl.ExtDlgs, IdHashMessageDigest, idHash,
  USelEmpresasLicencia, Vcl.CheckLst, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, System.IOUtils, Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc,
  Vcl.DBCtrls,ULogin,System.Generics.Collections, UPassword;

type
  TFLicenciaArchivo = class(TForm)
    txtArchivo: TEdit;
    btnActivar: TButton;
    odArchivo: TOpenTextFileDialog;
    btnDialog: TButton;
    btnDescargar: TButton;
    cbNumEmpresas: TComboBox;
    Label1: TLabel;
    gbSolicitud: TGroupBox;
    gbActivar: TGroupBox;
    Memo1: TMemo;
    svArchivo: TSaveTextFileDialog;
    cblEmpresas: TCheckListBox;
    odMspRuta: TFileOpenDialog;
    Conexion: TFDConnection;
    Label2: TLabel;
    txtPwd: TEdit;
    qryEmpresas: TFDQuery;
    lblNumEmpresas: TLabel;
    mtConexiones: TFDMemTable;
    dsConexiones: TDataSource;
    mtConexionesNombre: TStringField;
    mtConexioneshost: TStringField;
    mtConexionestipo: TIntegerField;
    mtConexionesruta: TStringField;
    Label3: TLabel;
    cbConexiones: TComboBox;
    btnCargarEmpresas: TButton;
    gbConexion: TGroupBox;
    CheckBox1: TCheckBox;
    btnBorrarRegistros: TButton;
    Button1: TButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Memo2: TMemo;
    procedure btnActivarClick(Sender: TObject);
    procedure btnDialogClick(Sender: TObject);
    procedure btnDescargarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnRutaMspClick(Sender: TObject);
    procedure cbNumEmpresasChange(Sender: TObject);
    function GetnumEmpresas: integer;
    function GetEmpresas : string;
    procedure FormShow(Sender: TObject);
    procedure CargarConexiones;
    procedure btnCargarEmpresasClick(Sender: TObject);
    procedure cblEmpresasClick(Sender: TObject);
    procedure activarempresas(empresas : string);
    procedure CheckBox1Click(Sender: TObject);
    function Password(caption:string):string;
      procedure borrarregistros(empresas : string);
    procedure btnBorrarRegistrosClick(Sender: TObject);
    function DeleteEnvVar(const VarName: string): Integer;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    lic : TLicencia;
    archivo : TextFile;
    num_empresas : integer;
    Conexiones: TDictionary<String, String>;
  end;

var
  FLicenciaArchivo: TFLicenciaArchivo;

implementation

{$R *.dfm}

function TFLicenciaArchivo.Password(caption:string): string;
var
  Fpwd : TPassword;
begin
  ///
  Fpwd := TPassword.Create(nil);
  FPwd.lblCaption.Caption := caption;
  if Fpwd.ShowModal=mrOk then
  begin
    Result := Fpwd.txtPassword.Text;
  end;
end;

procedure TFLicenciaArchivo.activarempresas(empresas:string);
var
  emps : TstringList;
  i: Integer;
  vals : TstringList;
  con,pwd,empresa : string;
begin
  //
  emps := tstringList.Create;
  ExtractStrings([','],[],Pchar(empresas),emps);
  mtConexiones.Locate('nombre',cbConexiones.Text,[]);
  for i := 0 to emps.Count-1 do
  begin
  //********************contraseņas de las conexiones***************************
    con := Emps[i].Split(['&'])[0];
    empresa := Emps[i].Split(['&'])[1];
    mtConexiones.Locate('nombre',con,[]);
    if not conexiones.TryGetValue(con,pwd) then
    begin
//  pwd :=  InputBox('Contraseņa','Ingrese la contraseņa de la conexion '+con,'');
      pwd := Password('Ingrese la contraseņa de la conexion '+con);
      conexiones.Add(con,pwd);
    end;
    Conexion.Params.Database := mtConexionesruta.Value+'\'+empresa.Replace('"','')+'.fdb';
    Conexion.Params.Values['Server'] := mtConexioneshost.Value;
    Conexion.Params.Values['Password'] := pwd;
    try
    Conexion.Open;
    except
      ShowMessage('seleccione la conexion correcta e indique la contraseņa de sysdba para poder activar las empresas');
    end;
    Conexion.ExecSQL('insert into registry values(-1,''SIC_POLIZAS'',''V'',0,'''+lic.EncryptStr(Conexion.Params.Database,lic.StringToWord('emp'))+''',NULL,null)');
    Conexion.Close;
  end;
end;


procedure TFLicenciaArchivo.btnActivarClick(Sender: TObject);
var
  txtcont : string;
  se : TLogin;
  b : boolean;
begin
  if txtArchivo.Text <> '' then
  begin
    assignFile(archivo,odArchivo.FileName);
    Reset(archivo);
    Read(archivo,txtcont);
    //VALIDA LA DECRIPTACION
    if lic.DecryptStr(txtcont,lic.stringtoword('enc')).Split([';'])[1] = trim(lic.GetHddSerialNumber) then
    begin
      b := lic.SetGlobalEnvironment('sic_polizas_pwd',txtcont);
      activarempresas(lic.DecryptStr(txtcont,lic.stringtoword('enc')).Split([';'])[2]);
      se := TLogin.Create(nil);
      se.Show;
      Hide;
    end
    else
    begin
      showmessage('El archivo de activacion no es el correcto');
    end;
  end;
end;

procedure TFLicenciaArchivo.btnBorrarRegistrosClick(Sender: TObject);
var
  txtcont : string;
  se : TLogin;
  b : boolean;
begin
  if txtArchivo.Text <> '' then
  begin
    assignFile(archivo,odArchivo.FileName);
    Reset(archivo);
    Read(archivo,txtcont);
    //VALIDA LA DECRIPTACION
    if lic.DecryptStr(txtcont,lic.stringtoword('enc')).Split([';'])[1] = trim(lic.GetHddSerialNumber) then
    begin
     // b := lic.SetGlobalEnvironment('sic_polizas_pwd',txtcont);
      borrarregistros(lic.DecryptStr(txtcont,lic.stringtoword('enc')).Split([';'])[2]);
      //ShowMessage('Registros borrados correctamente.');
    end
    else
    begin
      showmessage('El archivo de activacion no es el correcto.');
    end;
  end
  else
  showmessage('Favor de seleccionar archivo de activacion con las empresas seleccionadas.');
end;

procedure TFLicenciaArchivo.borrarregistros(empresas:string);
var
  emps : TstringList;
  i: Integer;
  vals : TstringList;
  value:string;
  b:bool;
  con,pwd,empresa,validar : string;
begin
 //
  emps := tstringList.Create;
  ExtractStrings([','],[],Pchar(empresas),emps);
  mtConexiones.Locate('nombre',cbConexiones.Text,[]);
  for i := 0 to emps.Count-1 do
  begin
  //********************contraseņas de las conexiones***************************
    con := Emps[i].Split(['&'])[0];
    empresa := Emps[i].Split(['&'])[1];
    mtConexiones.Locate('nombre',con,[]);
    if not conexiones.TryGetValue(con,pwd) then
    begin
//      pwd :=  InputBox('Contraseņa','Ingrese la contraseņa de la conexion '+con,'');
      pwd := Password('Ingrese la contraseņa de la conexion '+con);
      conexiones.Add(con,pwd);
    end;
    Conexion.Params.Database := mtConexionesruta.Value+'\'+empresa.Replace('"','')+'.fdb';
    Conexion.Params.Values['Server'] := mtConexioneshost.Value;
    Conexion.Params.Values['Password'] := pwd;
    try
    Conexion.Open;
    except
      ShowMessage('seleccione la conexion correcta e indique la contraseņa de sysdba para poder borrar los registros');
    end;
    validar:=conexion.ExecSQLScalar('select valor from registry where upper(nombre)=''SIC_POLIZAS''');
    if validar<>'' then
    begin
    Conexion.ExecSQL('delete from registry where upper(nombre)=''SIC_POLIZAS''');
    value := GetEnvironmentVariable('sic_polizas_pwd');
    if value<>'' then
      begin
      b := lic.SetGlobalEnvironment('sic_polizas_pwd','');
     // SetEnvironmentVariable('sic_polizas_pwd','');
      ShowMessage('Variable de entorno eliminada.');
      end;
    ShowMessage('Registro eliminado correctamente.');
    end
    else
    ShowMessage('El registro esta vacio.');
    Conexion.Close;
  end;
end;

procedure TFLicenciaArchivo.btnCargarEmpresasClick(Sender: TObject);
var
  ruta,hostname : string;
begin
    //cblEmpresas.Items.Clear;
    mtConexiones.Locate('nombre',cbConexiones.Text);
    ruta := mtConexionesruta.Value;
    hostname := mtConexioneshost.Value;
    Conexion.Params.Database := ruta+'System\Config.fdb';
    Conexion.Params.Values['Server'] := hostname;
    Conexion.Params.Values['Password'] := txtPwd.Text;
    Conexion.Open;
    qryEmpresas.Open;
    qryEmpresas.First;
    while not qryEmpresas.Eof do
    begin
      cblEmpresas.items.Add(cbConexiones.Text+'&'+qryEmpresas.FieldByName('nombre_corto').AsString);
      qryEmpresas.Next;
    end;
    Conexion.Close;
    lblNumEmpresas.Caption := inttostr(cblEmpresas.Items.Count)+' Empresas/ '+
                            inttostr(GetnumEmpresas)+' Seleccionadas';
end;

procedure TFLicenciaArchivo.btnDescargarClick(Sender: TObject);
var
  contenido : string;
  archivo_sol : TextFile;
begin
  if GetnumEmpresas>num_empresas then
  begin
    showmessage('El numero de Empresas es superior a las solicitadas en el candado');
  end
  else
  begin
    contenido := lic.GetHDDSerialNumber+';'+cbNumEmpresas.Text+';'+getempresas;
    contenido:=StringReplace(contenido,'"','',[rfReplaceAll, rfIgnoreCase]);
    contenido := lic.EncryptStr(contenido,lic.stringtoword('asic'));
    if svArchivo.Execute then
    begin
      AssignFile(archivo_sol,svArchivo.FileName+'.sic');
      ReWrite(archivo_sol);
      Write(archivo_sol,contenido);
      CloseFile(archivo_sol);
    end;
  end;
  
end;

procedure TFLicenciaArchivo.btnDialogClick(Sender: TObject);
begin
  if odArchivo.Execute then
  begin
    txtArchivo.Text := odArchivo.FileName;
     btnBorrarRegistros.Visible:=true;
     label10.Visible:=true;
  end;
end;

procedure TFLicenciaArchivo.btnRutaMspClick(Sender: TObject);
var
  ruta,hostname : string;
begin
  if odMspRuta.Execute then
  begin
    Conexion.Params.Database := ruta+'System\Config.fdb';
    Conexion.Params.Values['Hostname'] := hostname;
    Conexion.Params.Values['Password'] := txtPwd.Text;
    Conexion.Open;
    qryEmpresas.Open;
    qryEmpresas.First;
    while not qryEmpresas.Eof do
    begin
      cblEmpresas.items.Add(qryEmpresas.FieldByName('nombre_corto').AsString);
      qryEmpresas.Next;
    end;
    Conexion.Close;

  end;
end;

procedure TFLicenciaArchivo.Button1Click(Sender: TObject);
var
  txtcont,empresas : string;
begin
txtcont:=GetEnvironmentVariable('sic_polizas_pwd');
if txtcont='' then begin ShowMessage('No hay variable de entorno.');end
else
  begin
  empresas:=lic.DecryptStr(txtcont,lic.stringtoword('enc')).Split([';'])[2];
  empresas:=stringreplace(empresas,',',''+#13#10,[rfReplaceAll, rfIgnoreCase]);
  ShowMessage('Empresas Registradas:'+#13#10+stringreplace(empresas,'"','',[rfReplaceAll, rfIgnoreCase]));
  end;
  memo2.Visible:=true;
//ShowMessage(''+trim(lic.GetHddSerialNumber));

end;

function TFLicenciaArchivo.GetEmpresas : string;
var
  i,sel : integer;
  empresas : TStringlist;
begin
  empresas := TStringList.Create;
  sel  := 0;
  for i := 0 to cblEmpresas.Items.Count -1 do
  begin
    if cblEmpresas.Checked[i] then
      empresas.Add(cblEmpresas.Items[i]);
  end;
  empresas.Delimiter := ',';
  Result := empresas.DelimitedText;
end;

function TFLicenciaArchivo.GetnumEmpresas: integer;
var
  i,sel : integer;
begin
  sel  := 0;
  for i := 0 to cblEmpresas.Items.Count -1 do
  begin
    if cblEmpresas.Checked[i] then
      sel := sel + 1;
  end;
  Result := sel;
end;

procedure TFLicenciaArchivo.cblEmpresasClick(Sender: TObject);
begin
  lblNumEmpresas.Caption := inttostr(cblEmpresas.Items.Count)+' Empresas/ '+
                            inttostr(GetnumEmpresas)+' Seleccionadas';
end;

procedure TFLicenciaArchivo.cbNumEmpresasChange(Sender: TObject);
begin
  if cbNumEmpresas.ItemIndex = cbNumEmpresas.Items.Count-1 then
    num_empresas := 500
  else
    num_empresas := StrToInt(cbNumEmpresas.Text);
end;

procedure TFLicenciaArchivo.CheckBox1Click(Sender: TObject);
var
  i: Integer;
begin
  cblEmpresas.CheckAll(CheckBox1.State);
  lblNumEmpresas.Caption := inttostr(cblEmpresas.Items.Count)+' Empresas/ '+
                            inttostr(GetnumEmpresas)+' Seleccionadas';
end;

procedure TFLicenciaArchivo.FormCreate(Sender: TObject);
begin
  lic := TLicencia.Create;
  num_empresas := 1;
end;

procedure TFLicenciaArchivo.CargarConexiones;
var
  doc : IXMLDocument;
  conexiones : IXMLNodeList;
  cn,conexion : IXMLNode;
  i,tipo: Integer;
  nombre,ruta,host : string;
begin
  //
  doc := TXMLDocument.Create(nil);

  doc.Active := true;
  doc.LoadFromFile(TPath.GetPublicPath+'\Microsip\ProgramData.xml');
  cn := doc.ChildNodes.Get(1);
  conexiones := cn.childnodes.get(0).childnodes.get(0).childnodes.get(0).childnodes.get(1).ChildNodes;
    conexiones := cn.childnodes.get(0).childnodes.get(0).childnodes.get(0).childnodes['Conexiones'].ChildNodes;
  for i := 0 to conexiones.Count-1 do
  begin
    conexion := conexiones.Nodes[i];
    nombre := conexion.NodeName.Replace('_',' ').Trim;
    tipo := conexion.ChildNodes.Get(0).NodeValue;
    if tipo = 1 then
      host := 'localhost'
    else
      host := conexion.ChildNodes.Get(1).NodeValue;
    ruta := conexion.ChildNodes.Get(3).NodeValue;

    mtConexiones.Insert;
    mtConexionesNombre.Value := nombre;
    mtConexioneshost.Value := host;
    mtConexionestipo.Value := tipo;
    mtConexionesruta.Value := ruta;
    mtConexiones.Post;
    cbConexiones.Items.Add(nombre);
  end;
  //doc.AddChild('s');
  mtConexiones.Refresh;
  mtConexiones.First;
  cbConexiones.ItemIndex := 0;
end;

procedure TFLicenciaArchivo.FormShow(Sender: TObject);
begin
  cargarConexiones;
  Conexiones := TDictionary<String,String>.Create;
end;

function TFLicenciaArchivo.DeleteEnvVar(const VarName: string): Integer;
begin
  if SetEnvironmentVariable(PChar(VarName), nil) then
    Result := 0
  else
    Result := GetLastError;
end;

end.
